# Skill Matcher

People in the church have many skills, both personal and professional. Ministries have need of these skills. We aim to connect people with ministries that could benefit from their efforts.