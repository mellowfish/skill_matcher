Rails.application.routes.draw do

  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all

  resources :members, :except => [:index] do
    collection do
      match "login", :to => "members#login", :via => :all
      get :logout
      get :profile
    end

    resources :skills, :only => [:index], :controller => "member/skills" do
      member do
        get :select
        get :deselect
      end
    end
  end

  resources :skills
  resources :ministries

  get "/admin" => 'admin#index'

  root :to => redirect('/members/profile')
end
