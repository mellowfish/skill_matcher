class Member < ApplicationRecord
  include ActionView::Helpers

  has_secure_password

  has_many :member_skills
  has_many :skills, :through => :member_skills
  has_many :ministry_skills, :through => :skills
  has_many :ministries, :through => :ministry_skills

  validate :invalid_password?

  def self.current
    RequestStore.store[:member]
  end

  def self.current=(member)
    RequestStore.store[:member] = member
  end

  def self.create_from_params(params)
    member = self.find_by(:email => params[:email])
    if member
      if member.authenticate_with_params(**params)
        member.update(params)
      end
      member
    else
      self.create(params)
    end
  end

  def self.authenticate_with_params(email:, password:, **params)
    member = Member.find_by_email(email)
    if member.valid?
      unless member.authenticate_with_params(password: password)
        member = Member.new(email: email, password: password)
      end
    end
    member
  end

  def self.find_by_email(email)
    member = self.find_by(:email => email)
    if member.blank?
      member = Member.null(email: email)
    end
    member
  end

  def authenticate_with_params(password:, **params)
    self.authenticate(password)
  end

  def self.null(**data)
    member = Member.new(data)
    member.errors[:base] = I18n.t(:member_not_found)
    member
  end

  def possible_admin?
    admin? || true
  end

  def admin?
    true
  end

  def invalid_password?
    if !persisted? && (password.present? || password_digest.present?)
      self.errors[:password] = I18n.t("invalid_password")
    end
  end

  def member_skill_for_skill(skill)
    member_skills.where(:skill => skill).first
  end

  def has_skill?(skill)
    member_skill_for_skill(skill).present?
  end

  def has_ancestor_of_skill?(skill)
    member_skills.where(:skill => skill.ancestor_skills(true)).any?
  end

  def select_skill(skill)
    unless has_skill?(skill)
      skill.ancestor_skills(true).each do |ancestor_skill|
        skill = member_skills.find_by(:skill => ancestor_skill)
        unless skill
          member_skills.create(:skill => ancestor_skill)
        end
      end
    end
  end

  def deselect_skill(skill)
    if has_skill?(skill)
      member_skills.where(:skill => (skill.descendent_skills(true))).delete_all
    end
  end

  # TOOD: move all methods below this to a decorator
  def full_name
    first_name + " " + last_name
  end

  def tenure
    distance_of_time_in_words_to_now(created_at)
  end

end
