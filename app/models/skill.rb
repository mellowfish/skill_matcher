class Skill < ApplicationRecord
  belongs_to :parent, :class_name => "Skill", :required => false
  has_many :children, :foreign_key => :parent_id, :class_name => "Skill"

  has_many :ministry_skills
  has_many :ministries, :through => :ministry_skills

  has_many :member_skills
  has_many :members, :through => :member_skills

  scope :ordered, -> { order(:title => :desc) }
  scope :top_level, -> { ordered.where(:parent_id => nil) }

  before_save :update_slug

  def self.create_from_params(params)
    skill = self.find_or_initialize_by(title: params[:title])
    skill.update_from_params(params)
    skill
  end

  def update_from_params(params)
    update(params)
  end

  def to_param
    self.slug
  end

  def top_level?
    parent_id.nil?
  end

  def leaf?
    children.none?
  end

  def has_ancestor?(ancestor, include_self=false)
    ancestor_skills(include_self).include?(ancestor)
  end

  def ancestor_skills(include_self=false)
    skills = Array(parent.try(:ancestor_skills, true))
    if include_self
      skills << self
    end
    skills.compact
  end

  def descendent_skills(include_self=false)
    skills =
      if include_self
        [self]
      else
        []
      end
    skills.concat(children.flat_map { |child| child.descendent_skills(true) }).compact
  end

  private

  def update_slug
    self.slug = title.parameterize
  end
end
