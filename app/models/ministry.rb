class Ministry < ApplicationRecord
  has_many :ministry_skills
  has_many :skills, :through => :ministry_skills
  has_many :member_skills, :through => :skills
  has_many :members, :through => :member_skills

  scope :ordered, -> { order(:name => :desc) }

  before_save :update_slug

  def self.create_from_params(params)
    ministry = self.find_or_initialize_by(name: params[:name])
    ministry.update_from_params(params)
    ministry
  end

  def update_from_params(params)
    update(params)
  end

  def to_param
    self.slug
  end

  private

  def update_slug
    self.slug = self.name.parameterize
  end
end
