module ApplicationHelper

  def title
    content_for(:title) || t("title", :scope => "#{controller_name}.#{action_name}", :default => "Skill Matcher")
  end

  def display_class_for_member_skill(skill, member=current_member)
    member_skill = member.member_skill_for_skill(skill)
    if member_skill
      "active"
    else
      if @selected_skill
        if @selected_skill == skill
          "selected"
        elsif skill.top_level? || member.has_ancestor_of_skill?(skill)
          "related"
        elsif @selected_skill.try(:has_ancestor?, skill) || skill.parent == @selected_skill
          "open"
        else
          "inactive"
        end
      elsif skill.top_level? || member.has_ancestor_of_skill?(skill)
        "related"
      else
        "inactive"
      end
    end
    # TODO: have partial class
  end

end
