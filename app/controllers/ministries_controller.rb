class MinistriesController < ApplicationController
  before_filter :load_ministry, :only => [:show, :edit, :update]

  def index
    @ministries = Ministry.ordered
  end

  def new
    @ministry = Ministry.new
  end

  def create
    @ministry = Ministry.create_from_params(ministry_params)
    if @ministry.valid?
      redirect_to ministry_path(@ministry)
    else
      render "new"
    end
  end

  def show
  end

  def edit
  end

  def update
    @ministry.update_from_params(ministry_params)
    if @ministry.valid?
      redirect_to ministry_path(@ministry)
    else
      render "edit"
    end
  end

  private

  def load_ministry
    @ministry = Ministry.find_by_slug(params[:id])
  end

  def ministry_params
    params
      .require(:ministry).permit(:name, :description)
      .to_h
      .symbolize_keys
  end
end