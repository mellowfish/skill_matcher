# module Member; end

class Member::SkillsController < ApplicationController
  before_filter :load_skills

  def index
  end

  def select
    if @selected_skill
      current_member.select_skill(@selected_skill)
    end
    render "index"
  end

  def deselect
    if @selected_skill
      current_member.deselect_skill(@selected_skill)
    end
    render "index"
  end

  private

  def load_skills
    @skills = Skill.top_level
    @selected_skill = Skill.find_by_slug(params[:id])
  end

end