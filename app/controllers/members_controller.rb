class MembersController < ApplicationController

  def new
    @member = Member.new
  end

  def create
    @member = Member.create_from_params(member_params)
    if @member.valid?
      session[:member_id] = @member.id
      redirect_to profile_members_path
    else
      render "new"
    end
  end

  def login
    if current_member.present?
      return redirect_to profile_members_path
    end

    if params[:member].present?
      @password = member_params[:password]
      @member = Member.authenticate_with_params(member_params)
      if @member.valid?
        session[:member_id] = @member.id
        return redirect_to profile_members_path
      end
    else
      @member = Member.new
    end
  end

  def logout
    session.destroy
    Member.current = nil
    redirect_to login_members_path
  end

  def show
    @member = Member.find_by(:id => params[:id])
    raise_not_found unless @member
    return redirect_to profile_members_path if @member == current_member
  end

  def profile
    return redirect_to(login_members_path) unless current_member.present?
  end

  private

  def member_params
    params
      .require(:member).permit(:email, :password, :password_confirmation, :first_name, :last_name)
      .to_h
      .symbolize_keys
  end
end