class SkillsController < ApplicationController
  before_filter :load_skill, :only => [:show, :edit, :update]

  def index
    @skills = Skill.ordered
  end

  def new
    @skill = Skill.new
  end

  def create
    @skill = Skill.create_from_params(skill_params)
    if @skill.valid?
      redirect_to skill_path(@skill)
    else
      render "new"
    end
  end

  def show
  end

  def edit
  end

  def update
    @skill.update_from_params(skill_params)
    if @skill.valid?
      redirect_to skill_path(@skill)
    else
      render "edit"
    end
  end

  private

  def load_skill
    @skill = Skill.find_by_slug(params[:id])
    raise_not_found unless @skill
  end

  def skill_params
    params
      .require(:skill).permit(:title, :member_description, :ministry_description, :shared_description)
      .to_h
      .symbolize_keys
  end
end