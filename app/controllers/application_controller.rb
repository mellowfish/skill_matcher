class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_member
    Member.current || (Member.current = Member.find_by_id(session[:member_id]))
  end
  helper_method :current_member

  def raise_not_found
    render("/errors/not_found", :status => 404)
  end
end
