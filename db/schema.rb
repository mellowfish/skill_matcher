# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160312044548) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "member_skills", force: :cascade do |t|
    t.integer  "member_id"
    t.integer  "skill_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "member_skills", ["member_id"], name: "index_member_skills_on_member_id", using: :btree
  add_index "member_skills", ["skill_id"], name: "index_member_skills_on_skill_id", using: :btree

  create_table "members", force: :cascade do |t|
    t.string   "email"
    t.string   "password_digest"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "ministries", force: :cascade do |t|
    t.string   "name"
    t.string   "slug"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "ministry_skills", force: :cascade do |t|
    t.integer  "ministry_id"
    t.integer  "skill_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "ministry_skills", ["ministry_id"], name: "index_ministry_skills_on_ministry_id", using: :btree
  add_index "ministry_skills", ["skill_id"], name: "index_ministry_skills_on_skill_id", using: :btree

  create_table "skills", force: :cascade do |t|
    t.string   "title"
    t.string   "slug"
    t.integer  "parent_id"
    t.text     "ministry_description"
    t.text     "member_description"
    t.text     "shared_description"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "skills", ["parent_id"], name: "index_skills_on_parent_id", using: :btree

  add_foreign_key "member_skills", "members"
  add_foreign_key "member_skills", "skills"
  add_foreign_key "ministry_skills", "ministries"
  add_foreign_key "ministry_skills", "skills"
end
