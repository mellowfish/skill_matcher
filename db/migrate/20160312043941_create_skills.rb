class CreateSkills < ActiveRecord::Migration[5.0]
  def change
    create_table :skills do |t|
      t.string :title
      t.string :slug
      t.references :parent
      t.text :ministry_description
      t.text :member_description
      t.text :shared_description

      t.timestamps
    end
  end
end
