class CreateMinistrySkills < ActiveRecord::Migration[5.0]
  def change
    create_table :ministry_skills do |t|
      t.references :ministry, foreign_key: true
      t.references :skill, foreign_key: true

      t.timestamps
    end
  end
end
