class CreateMemberSkills < ActiveRecord::Migration[5.0]
  def change
    create_table :member_skills do |t|
      t.references :member, foreign_key: true
      t.references :skill, foreign_key: true

      t.timestamps
    end
  end
end
