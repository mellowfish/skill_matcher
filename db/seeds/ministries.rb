ministries_data = [
  {
    :name =>
      "Youth",
  },
  {
    :name =>
      "Bible Study",
  },
  {
    :name =>
      "Soup Kitchen",
  },
  {
    :name =>
      "Small Groups",
  },
]

ministries_data.each do |ministry_data|
  Ministry.create_from_params(ministry_data)
  puts "Seeding Ministry - #{ministry_data[:name]}"
end