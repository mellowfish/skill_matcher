skills_data = [
  {
    :title =>
      "Servant",
    :member_description =>
      "I enjoy serving other people. Their success brings me happiness.",
    :children => [
      {
        :title =>
          "Compassion",
        :ministry_description =>
          "Your members care deeply about people. When people struggle, hurt, or are in need, your members feel their pain. When people succeed your members rejoice with them.",
        :member_description =>
          "I care deeply about people. When they struggle, hurt or are in need, I feel their pain. When they succeed I rejoice with them.",
      },
      {
        :title =>
          "Volunteering",
        :member_description =>
          "I enjoy giving of my time to help other people. I keep an open schedule so I can be available when people need me.",
      },
    ],
  },
  {
    :title =>
      "Shepherd",
    :member_description =>
      "I have a group of people that I oversee and minister to. They trust me to meet their needs and handle their problems.",
    :children => [
      {
        :title =>
          "Organization",
        :member_description =>
          "I like to create and manage systems, events, and groups of people. I enjoy tracking all the little details that go into a successful enterprise",
      },
      {
        :title =>
          "Leadership",
        :member_description =>
          "I frequently find myself the head of a group of people. I can inspire a common purpose and keep people moving in the same direction.",
      },
    ],
  },
  {
    :title =>
      "Saint",
    :member_description =>
      "I am centered in Christ, He is my strength. I enjoy sharing my life with Him, and watching him solve insurmountable problems.",
    :children => [
      {
        :title =>
          "Patience",
        :member_description =>
          "I don't get frustrated easily. I enjoy challenging situations.",
      },
      {
        :title =>
          "Prayer",
        :member_description =>
          "I like to lift my concerns and those of others up to God in prayer. I like leading groups in prayer or praying with individuals.",
      },
    ],
  },
  {
    :title =>
      "Scribe",
    :member_description =>
      "I find great value in the Bible. I enjoy sharing it's riches with others.",
    :children => [
      {
        :title =>
          "Teaching",
        :member_description =>
          "I enjoy public speaking and teaching. I can prepare a lesson on a topic and help others learn it well.",
      },
      {
        :title =>
          "Scripture Knowledge",
        :member_description =>
          "I regularly read and study the Bible. I am frequently sought out for my opinion on bible passages, and can give complex answers to tough questions.",
      },
    ],
  },
  {
    :title =>
      "Spotter",
    :member_description =>
      "I enjoy coming alongside someone who is struggling and giving aid. People come to me for advice.",
    :children => [
      {
        :title =>
          "Encourager",
        :member_description =>
          "I enjoy lifting others up with praise and positive criticism. I am able to strengthen the faith of a fellow believer and convince them that their troubles will not consume them."
      },
      {
        :title =>
          "Guidance",
        :member_description =>
          "I can provide wise, biblical counsel in a number of areas. I help people understand how they can improve their walk with Christ, or how to choose the best path when they are at a turning point.",
      },
    ]
  },
]

def add_skills(skills_data, parent_skill=nil)
  skills_data.each do |skill_data|
    children = skill_data.delete(:children)
    if parent_skill
      skill_data.merge!(:parent => parent_skill)
    end
    skill = Skill.create_from_params(skill_data)
    puts "Seeding Skill - #{skill_data[:title]}"

    if children.present?
      add_skills(children, skill)
    end
  end
end

add_skills(skills_data)