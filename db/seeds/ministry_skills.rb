ministry_skills_data = {
  "Youth" => [
    "Compassion",
    "Patience",
    "Organization",
    "Teaching",
    "Encourager",
    "Guidance",
  ],
  "Bible Study" => [
    "Teaching",
    "Scripture Knowledge",
    "Organization"
  ],
  "Soup Kitchen" => [
    "Organization",
    "Compassion",
    "Volunteering"
  ],
  "Small Groups" => [
    "Organization",
    "Leadership",
    "Prayer",
    "Guidance",
  ],
}

MinistrySkill.delete_all
ministry_skills_data.each do |ministry_name, skill_titles|
  ministry = Ministry.find_by_name(ministry_name)
  if ministry
    skill_titles.each do |skill_title|
      skill = Skill.find_by_title(skill_title)
      if skill
        ministry.skills << skill
        puts "Seeding MinistrySkill: #{ministry_name} => #{skill_title}"
      else
        puts "Failed to find Skill: #{skill_title}"
      end
    end
  else
    puts "Failed to seed MinistrySkills for Ministry: #{ministry_name}"
  end
end